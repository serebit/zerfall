attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;

uniform mat4 u_projTrans;
uniform mat4 u_projModelView;

varying vec4 v_color;
varying vec2 v_texCoords;

const mat4 empty_matrix = mat4(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

void main() {
    v_color = a_color;
    v_texCoords = a_texCoord0;
    // if the modelView is empty, it's being called from SpriteBatch, so set the position accordingly
    if (u_projModelView == empty_matrix) {
        gl_Position = u_projTrans * a_position;
    } else { // otherwise, it's being called from ShapeRenderer, so set the position accordingly
        gl_Position = u_projModelView * a_position;
        gl_PointSize = 1.0;
    }
}
