package com.serebit.zerfall.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Align
import com.serebit.extensions.gdx.glClearColor
import com.serebit.zerfall.game.maps.Map
import com.serebit.zerfall.game.world.objects.*
import com.serebit.zerfall.utilities.assets.AssetManager
import com.serebit.zerfall.utilities.assets.loadSound
import com.serebit.zerfall.utilities.graphics.Renderer
import kotlin.math.pow
import kotlin.math.roundToInt

class Game(map: Map, private val assetManager: AssetManager) {
    private val world: World = map.world
    private val objects: MutableSet<GameObject> = map.objects.toMutableSet()
    private val player = objects.filterIsInstance<Player>().firstOrNull()
    private var round = 1
    private val roundZombies get() = 4 + 4 * round
    private var remainingZombies = roundZombies
    private var spawnedZombies = 0
    private val roundStartSound = loadSound("sounds/round-start.ogg")
    private val pixelsPerMeter = 80f
    private val worldRenderer = Renderer(
        OrthographicCamera(Gdx.graphics.width / pixelsPerMeter, Gdx.graphics.height / pixelsPerMeter)
    )
    private val hudRenderer = Renderer(
        OrthographicCamera(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
    )

    fun update(delta: Float) {
        world.step(delta, 6, 2)
        val objectsCopy = objects.toSet()
        objectsCopy.forEach {
            it.update(delta, world, objects)
            if (it is Spawner && it.isReady && spawnedZombies < roundZombies) {
                spawnedZombies++
                it.spawn(
                    Zombie(
                        world,
                        it.position,
                        (75 + 50 * 1.5f.pow(round)).roundToInt().toFloat()
                    ),
                    objects
                )
            }
        }

        objectsCopy.forEach {
            if (it is Character && it.health <= 0) {
                if (it is Zombie) remainingZombies--
                world.destroyBody(it.body)
                objects.remove(it)
            }
        }

        if (remainingZombies == 0) {
            round++
            remainingZombies = roundZombies
            spawnedZombies = 0
            roundStartSound.play()
        }

        player?.also {
            worldRenderer.position.interpolate(
                Vector3(it.body.position.x, it.body.position.y, worldRenderer.position.z),
                delta * 10,
                Interpolation.linear
            )
            worldRenderer.position.x = it.body.position.x
        }
    }

    fun render() {
        worldRenderer.update()
        hudRenderer.update()
        worldRenderer.gl {
            glClearColor(0x212121FF)
            glClear(GL20.GL_COLOR_BUFFER_BIT)
        }

        worldRenderer.update()
        objects.sortedBy { it.zIndex }.forEach { it.render(worldRenderer) }
        //worldRenderer.drawDebug(world)

        // draw HUD
        player?.also {
            // draw player health bar
            hudRenderer.fillRect(
                x = 0f, y = -hudRenderer.height / 2f + 20f,
                width = 250f, height = 40f,
                color = Color.GREEN
            )
            // draw player gun name
            hudRenderer.drawText(
                it.gun.name,
                x = hudRenderer.width / 2 - 15f, y = -hudRenderer.height / 2 + 70f,
                horizontalAlignment = Align.right,
                font = assetManager.getFont("FiraCode-Regular", 24)
            )
            // draw player ammo count
            hudRenderer.drawText(
                "${it.gun.magazine.magazineAmmo} / ${it.gun.magazine.reserveAmmo}",
                hudRenderer.width / 2f - 15f,
                -hudRenderer.height / 2f + 40f,
                horizontalAlignment = Align.right,
                font = assetManager.getFont("FiraCode-Regular", 32)
            )
            // draw player score
            hudRenderer.drawText(
                "${it.score}",
                -hudRenderer.width / 2f + 15f,
                0f,
                horizontalAlignment = Align.left,
                font = assetManager.getFont("FiraCode-Regular", 24)
            )
        }

        // draw zombie health
        objects.filterIsInstance<Zombie>().forEach {
            hudRenderer.drawText(
                it.health.toString(),
                (-worldRenderer.position.x + it.body.position.x) * pixelsPerMeter,
                (-worldRenderer.position.y + it.body.position.y) * pixelsPerMeter,
                horizontalAlignment = Align.center,
                font = assetManager.getFont("FiraCode-Regular", 20)
            )
        }

        objects.filterIsInstance<Sensor>().filter { it.isActive }.forEach {
            hudRenderer.drawText(
                it.prompt,
                0f,
                -120f,
                horizontalAlignment = Align.center,
                font = assetManager.getFont("FiraCode-Regular", 20)
            )
        }

        // draw round number
        hudRenderer.drawText(
            round.toString(),
            -hudRenderer.width / 2f + 16f,
            -hudRenderer.height / 2f + 72f,
            horizontalAlignment = Align.left,
            font = assetManager.getFont("DKFaceYourFearsII", 72),
            color = Color(0xFF3D00FF.toInt())
        )
    }
}