package com.serebit.zerfall.game.maps

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.game.guns.Kiparis
import com.serebit.zerfall.game.guns.M1Carbine
import com.serebit.zerfall.game.guns.Sterling
import com.serebit.zerfall.game.world.objects.GunDispenser
import com.serebit.zerfall.game.world.objects.Player
import com.serebit.zerfall.game.world.objects.Spawner
import com.serebit.zerfall.game.world.objects.Structure
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.geometry.Point3

class PrototypeMap : Map {
    override val world = World(Vector2(0f, -60f), false)
    private val player = Player(world, Point2(0f, 0f))
    override val objects = setOf(
        Spawner(world, Point3(-5f, .5f, -1f)),
        Spawner(world, Point3(5f, .5f, -1f)),
        player,
        GunDispenser(world, Point3(0f, .25f, -1f), player, 1000, M1Carbine()) { M1Carbine() },
        GunDispenser(world, Point3(0f, -4f, -1f), player, 1000, Kiparis()) { Kiparis() },
        Structure(world, Point3(0f, -5f, 0f), Vector2(40f, .5f), Color.GRAY),
        Structure(world, Point3(19f, 0f, 0f), Vector2(.5f, 10f), Color.GRAY),
        Structure(world, Point3(-19f, 0f, 0f), Vector2(.5f, 10f), Color.GRAY),
        Structure(world, Point3(0f, -1f, 0f), Vector2(25f, .5f), Color.GRAY)
    )
}