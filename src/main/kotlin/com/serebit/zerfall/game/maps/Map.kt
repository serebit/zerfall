package com.serebit.zerfall.game.maps

import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.game.world.objects.GameObject

interface Map {
    val world: World
    val objects: Set<GameObject>
}