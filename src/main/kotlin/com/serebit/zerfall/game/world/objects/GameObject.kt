package com.serebit.zerfall.game.world.objects

import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.graphics.Renderer

interface GameObject {
    val position: Point2 get() = Point2(body.position.x, body.position.y)
    val body: Body
    var zIndex: Float

    fun update(delta: Float, world: World, objects: MutableSet<GameObject>)

    fun render(renderer: Renderer)
}

interface DynamicObject : GameObject

interface StaticObject : GameObject