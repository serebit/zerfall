package com.serebit.zerfall.game.world.objects

interface Sensor {
    val prompt: String
    var isActive: Boolean
}

class SensorModule(private inline var onActivation: () -> String) : Sensor {
    override val prompt get() = onActivation()
    override var isActive: Boolean = false
}