package com.serebit.zerfall.game.world.objects

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.serebit.extensions.box2d.queryAABB
import com.serebit.extensions.gdx.asPoint2
import com.serebit.extensions.gdx.isKeyJustPressed
import com.serebit.extensions.gdx.isKeyPressed
import com.serebit.zerfall.game.guns.Gun
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.geometry.Point3
import com.serebit.zerfall.utilities.graphics.Renderer
import ktx.box2d.body
import kotlin.reflect.KClass

class GunDispenser<T : Gun>(
    world: World,
    position: Point3,
    player: Player,
    private val cost: Int,
    private val gunInstance: T,
    private inline val dispense: () -> T
) : GameObject, Sensor by SensorModule({
    if (player.gun::class != gunInstance::class) {
        "Press E to buy ${gunInstance.name} (Cost: $cost)"
    } else {
        "Press E to buy ammo (Cost: ${cost / 2})"
    }
}) {
    override val body: Body = world.body(BodyDef.BodyType.StaticBody) {
        this.position.set(position.x, position.y)
        box(width = 1f, height = .5f) {
            isSensor = true
        }
    }
    override var zIndex: Float = position.z

    override fun update(delta: Float, world: World, objects: MutableSet<GameObject>) {
        isActive = false
        world.queryAABB(body.position.asPoint2, Point2(1f, .5f)) { fixture ->
            (objects.find { fixture in it.body.fixtureList } as? Player)?.let { player ->
                isActive = true
                if (isKeyJustPressed(Input.Keys.E)) {
                    if (player.gun::class == gunInstance::class && player.score >= cost / 2) {
                        player.score -= cost / 2
                        player.gun.magazine.refillReserves()
                    } else if (player.score >= cost) {
                        player.score -= cost
                        player.gun = dispense()
                    }
                }
                false
            } ?: true
        }
    }

    override fun render(renderer: Renderer) {
        renderer.fillRect(
            body.position.x, body.position.y,
            width = 1f, height = .5f,
            color = Color.WHITE
        )
    }
}