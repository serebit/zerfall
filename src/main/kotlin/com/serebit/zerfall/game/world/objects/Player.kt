package com.serebit.zerfall.game.world.objects

import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.World
import com.serebit.extensions.box2d.group
import com.serebit.extensions.box2d.queryAABB
import com.serebit.extensions.gdx.asPoint2
import com.serebit.extensions.gdx.isKeyJustPressed
import com.serebit.extensions.gdx.isKeyPressed
import com.serebit.extensions.gdx.isKeyReleased
import com.serebit.zerfall.game.guns.Gun
import com.serebit.zerfall.game.guns.Ppk
import com.serebit.zerfall.utilities.FilterGroup
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.graphics.Renderer
import ktx.box2d.body
import ktx.math.vec2

class Player(world: World, position: Point2) : DynamicObject, Character by HealthModule(500f) {
    override val body: Body = world.body(BodyDef.BodyType.DynamicBody) {
        fixedRotation = true
        this.position.set(position.x, position.y)
        box(.75f, 2f) {
            density = 16f
            friction = .2f
            filter.group = FilterGroup.CHARACTER
        }
    }
    override var zIndex: Float = 1f

    var gun: Gun = Ppk()

    var totalScore: Int = 500
        private set
    var score: Int = 500
        set(value) {
            totalScore += (field - value.coerceAtLeast(0))
            field = value.coerceAtLeast(0)
        }

    override fun update(delta: Float, world: World, objects: MutableSet<GameObject>) {
        val foundFixtures = mutableSetOf<Fixture>()
        world.queryAABB(
            position = body.position.asPoint2 - Point2(0f, 1f),
            dimensions = Point2(.55f, .2f)
        ) {
            if (it !in body.fixtureList && it.filterData.group != FilterGroup.CHARACTER) {
                foundFixtures.add(it)
            } else true
        }
        val sprintMultiplier = if (isKeyPressed(Input.Keys.SHIFT_LEFT)) 2f else 1f
        when {
            isKeyPressed(Input.Keys.A) == isKeyPressed(Input.Keys.D) -> Unit
            isKeyPressed(Input.Keys.D) && body.linearVelocity.x < 6 * sprintMultiplier -> {
                body.applyForceToCenter(1200f, 0f, true)
            }
            isKeyPressed(Input.Keys.A) && body.linearVelocity.x > -6 * sprintMultiplier -> {
                body.applyForceToCenter(-1200f, 0f, true)
            }
        }
        if (isKeyJustPressed(Input.Keys.SPACE) && foundFixtures.isNotEmpty()) {
            body.applyLinearImpulse(vec2(0f, 600f), body.position, true)
        }

        var direction: Point2? = null

        if (gun.isNotEmpty) {
            direction = when {
                isKeyPressed(Input.Keys.J) -> {
                    Point2(-1f, 0f)
                }
                isKeyPressed(Input.Keys.K) -> {
                    Point2(1f, 0f)
                }
                else -> null
            }
        }
        if (isKeyJustPressed(Input.Keys.J) || isKeyJustPressed(Input.Keys.K)) {
            gun.pullTrigger()
        }
        if (isKeyReleased(Input.Keys.J) && isKeyReleased(Input.Keys.K)) {
            gun.releaseTrigger()
        }

        if ((isKeyJustPressed(Input.Keys.R) && gun.isNotFull) || gun.isEmpty && gun.state == Gun.State.IDLE) {
            gun.reload()
        }

        gun.update(delta, world, this, body.position.asPoint2, direction, objects)
    }

    override fun render(renderer: Renderer) {
        renderer.fillRect(
            body.position.x,
            body.position.y,
            .75f,
            2f,
            color = Color.GRAY
        )

        renderer.strokeRect(
            body.position.x, body.position.y,
            width = .75f, height = 2f,
            strokeWidth = 0.025f,
            color = Color.LIGHT_GRAY
        )

        gun.action.bulletPath?.run {
            renderer.drawLine(
                start.x, start.y, end.x, end.y, .05f, color = Color.ORANGE
            )
        }
    }

    override fun toString(): String = "Player(position=${body.position}, health=$health)"
}