package com.serebit.zerfall.game.world.objects

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.geometry.Point3
import com.serebit.zerfall.utilities.graphics.Renderer
import ktx.box2d.body

class Structure(
    world: World,
    position: Point3,
    private val dimensions: Vector2,
    private val color: Color
) : GameObject {
    override val body: Body = world.body {
        this.position.set(position.x, position.y)
        box(dimensions.x, dimensions.y) {
            density = 50f
            friction = 1f
        }
    }
    override var zIndex = position.z

    override fun update(delta: Float, world: World, objects: MutableSet<GameObject>) = Unit

    override fun render(renderer: Renderer) {
        renderer.fillRect(body.position.x, body.position.y, dimensions.x, dimensions.y, color)
    }
}