package com.serebit.zerfall.game.world.objects

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.serebit.extensions.box2d.group
import com.serebit.zerfall.utilities.FilterGroup
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.graphics.Renderer
import ktx.box2d.body
import ktx.math.times
import ktx.math.vec2
import kotlin.math.absoluteValue

const val TRACK_PLAYER = true

class Zombie(
    world: World,
    position: Point2,
    health: Float = 150f,
    private val maxSpeed: Float = 1f
) : DynamicObject, Character by HealthModule(health) {
    override val body: Body = world.body(BodyDef.BodyType.DynamicBody) {
        fixedRotation = true
        this.position.set(position.x, position.y)
        box(.75f, 2f) {
            density = 16f
            friction = .4f
            filter.group = FilterGroup.CHARACTER
        }
    }
    override var zIndex: Float = 1f

    override fun update(delta: Float, world: World, objects: MutableSet<GameObject>) {
        val closestPlayer = objects.filterIsInstance<Player>().sortedBy {
            it.body.position.dst(body.position)
        }.firstOrNull()

        closestPlayer?.let { player ->
            if (body.linearVelocity.x.absoluteValue < maxSpeed) {
                val vector = player.body.position.sub(body.position).nor() * vec2(1500f, 0f)
                if (TRACK_PLAYER) body.applyForceToCenter(vector, true)
            }
        }
    }

    override fun render(renderer: Renderer) {
        renderer.fillRect(
            body.position.x,
            body.position.y,
            .75f,
            2f,
            color = Color(0x33691EFF)
        )

        renderer.strokeRect(
            body.position.x,
            body.position.y,
            .75f, 2f,
            strokeWidth = 0.025f,
            color = Color(0x33691EFF).lerp(Color.WHITE, 0.25f)
        )
    }
}