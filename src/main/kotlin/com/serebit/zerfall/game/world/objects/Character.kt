package com.serebit.zerfall.game.world.objects

interface Character {
    var health: Float
}

class HealthModule(max: Float, initial: Float = max) : Character {
    override var health: Float = initial
        set(value) {
            field = value.coerceAtMost(value)
        }
}