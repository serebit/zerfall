package com.serebit.zerfall.game.world.objects

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.physics.box2d.Body
import com.badlogic.gdx.physics.box2d.BodyDef
import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.utilities.geometry.Point3
import com.serebit.zerfall.utilities.graphics.Renderer
import ktx.box2d.body

class Spawner(world: World, position: Point3) : GameObject {
    override val body: Body = world.body(BodyDef.BodyType.StaticBody) {
        this.position.set(position.x, position.y)
    }
    override var zIndex: Float = position.z
    private var spawnTimer: Float = 60f
    private var timerMaximum: Int = 5
    val isReady get() = spawnTimer >= timerMaximum

    override fun update(delta: Float, world: World, objects: MutableSet<GameObject>) {
        if (spawnTimer < timerMaximum) {
            spawnTimer += delta
        }
    }

    fun spawn(obj: GameObject, objects: MutableSet<GameObject>) {
        objects.add(obj)
        spawnTimer = 0f
    }

    override fun render(renderer: Renderer) {
        renderer.fillRect(body.position.x, body.position.y, 1.25f, 1.25f, Color.BLACK)
        renderer.strokeRect(
            body.position.x, body.position.y,
            1.25f, 1.25f,
            0.125f,
            Color(0x99582BFF.toInt())
        )
    }
}