package com.serebit.zerfall.game.guns

import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.game.guns.components.Action
import com.serebit.zerfall.game.guns.components.Magazine
import com.serebit.zerfall.game.world.objects.GameObject
import com.serebit.zerfall.utilities.geometry.Point2

interface Gun {
    val name: String
    val action: Action
    val magazine: Magazine
    val isEmpty get() = magazine.magazineAmmo <= 0
    val isNotEmpty get() = !isEmpty
    val isFull get() = magazine.magazineAmmo >= magazine.magazineCapacity
    val isNotFull get() = magazine.magazineAmmo < magazine.magazineCapacity
    val state: State get() = when {
        magazine.reloadTimer.isActive -> State.RELOADING
        action.isEnabled -> State.FIRING
        else -> State.IDLE
    }

    fun update(
        delta: Float,
        world: World,
        sourceObject: GameObject,
        sourcePoint: Point2,
        direction: Point2?,
        objects: Set<GameObject>
    ) {
        if (isEmpty) {
            action.isEnabled = false
        }
        magazine.update(delta)
        action.update(delta, sourceObject, sourcePoint, direction, world, objects) {
            magazine.magazineAmmo--
        }
    }

    fun pullTrigger() {
        if (state == State.IDLE && isNotEmpty) {
            action.isEnabled = true
        }
    }

    fun releaseTrigger() {
        action.isEnabled = false
    }

    fun reload() {
        magazine.reload()
    }

    enum class State {
        RELOADING,
        IDLE,
        FIRING
    }
}