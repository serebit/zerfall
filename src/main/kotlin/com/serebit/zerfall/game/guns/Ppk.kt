package com.serebit.zerfall.game.guns

import com.serebit.zerfall.game.guns.components.*

class Ppk : Gun {
    override val name: String = "Walther PPK"
    override val action: Action = SemiAutoAction(
        "sounds/ppk-gunshot.ogg",
        600,
        HitscanRound(500f, 40f)
    )
    override val magazine: Magazine = BasicMagazine(
        magazineCapacity = 8,
        reserveCapacity = 96,
        reloadDuration = 1.8f,
        reloadSoundPath = "sounds/ppk-reload.ogg"
    )
}