package com.serebit.zerfall.game.guns.components

import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.game.world.objects.GameObject
import com.serebit.zerfall.utilities.Timer
import com.serebit.zerfall.utilities.assets.loadSound
import com.serebit.zerfall.utilities.geometry.Line2
import com.serebit.zerfall.utilities.geometry.Point2

interface Action {
    val gunshotSound: Sound
    var bulletPath: Line2?
    var isEnabled: Boolean

    fun update(
        delta: Float,
        sourceObject: GameObject, sourcePoint: Point2,
        direction: Point2?,
        world: World,
        objects: Collection<GameObject>,
        onFire: (() -> Unit)? = null
    )
}

class SemiAutoAction(
    gunshotSoundPath: String,
    maxRoundsPerMinute: Int,
    private val round: Round
) : Action {
    override val gunshotSound = loadSound(gunshotSoundPath)
    override var bulletPath: Line2? = null
    override var isEnabled: Boolean = false
    private var isReadyToFire: Boolean = false
    private val readinessTimer = Timer(1f / (maxRoundsPerMinute / 60))
    private var soundId: Long = -1L

    override fun update(
        delta: Float,
        sourceObject: GameObject, sourcePoint: Point2,
        direction: Point2?,
        world: World,
        objects: Collection<GameObject>,
        onFire: (() -> Unit)?
    ) {
        bulletPath = null
        readinessTimer.update(delta)
        if (isEnabled && isReadyToFire && readinessTimer.isNotActive && direction != null) {
            gunshotSound.stop(soundId)
            soundId = gunshotSound.play()
            bulletPath = round.launch(sourceObject, sourcePoint, direction, world, objects)
            readinessTimer.reset()
            readinessTimer.start()
            onFire?.invoke()
            isReadyToFire = false
        }
        if (!isEnabled) {
            isReadyToFire = true
        }
    }
}

class FullAutoAction(
    gunshotSoundPath: String,
    fireRate: Int,
    private val round: Round
) : Action {
    override val gunshotSound: Sound = loadSound(gunshotSoundPath)
    override var bulletPath: Line2? = null
    override var isEnabled: Boolean = false
    private var isFiring: Boolean = false
    private var cycleTimer = Timer(1f / (fireRate / 60))
    private var soundId: Long = -1L

    override fun update(
        delta: Float,
        sourceObject: GameObject, sourcePoint: Point2,
        direction: Point2?,
        world: World,
        objects: Collection<GameObject>,
        onFire: (() -> Unit)?
    ) {
        bulletPath = null
        if (isEnabled && direction != null) {
            if (cycleTimer.isNotActive && !isFiring) {
                isFiring = true
                cycleTimer.start()
                gunshotSound.stop(soundId)
                soundId = gunshotSound.play()
                bulletPath = round.launch(sourceObject, sourcePoint, direction, world, objects)
                onFire?.invoke()
                cycleTimer.reset()
                cycleTimer.start()
            }
            if (cycleTimer.isDone) {
                gunshotSound.stop(soundId)
                soundId = gunshotSound.play()
                bulletPath = round.launch(sourceObject, sourcePoint, direction, world, objects)
                onFire?.invoke()
                cycleTimer.reset()
                cycleTimer.start()
            }
            cycleTimer.update(delta)
        } else {
            cycleTimer.reset()
            isFiring = false
        }
    }
}