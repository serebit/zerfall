package com.serebit.zerfall.game.guns

import com.serebit.zerfall.game.guns.components.*

class Sterling : Gun {
    override val name: String = "Sterling SMG"
    override val action: Action = FullAutoAction(
        "sounds/sterling-gunshot.ogg",
        550,
        HitscanRound(400f, 100f)
    )
    override val magazine: Magazine = BasicMagazine(
        34,
        204,
        2.7f,
        "sounds/m1carbine-reload.ogg" // TODO: Acquire Sterling reload
    )
}