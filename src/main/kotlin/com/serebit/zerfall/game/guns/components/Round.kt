package com.serebit.zerfall.game.guns.components

import com.badlogic.gdx.physics.box2d.World
import com.serebit.extensions.box2d.isNotSensor
import com.serebit.extensions.box2d.rayCast
import com.serebit.extensions.gdx.asPoint2
import com.serebit.zerfall.utilities.geometry.Line2
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.game.world.objects.Character
import com.serebit.zerfall.game.world.objects.GameObject
import com.serebit.zerfall.game.world.objects.Player

interface Round {
    val range: Float
    val damage: Float

    fun launch(
        sourceObject: GameObject,
        sourcePoint: Point2,
        direction: Point2,
        world: World,
        objects: Collection<GameObject>
    ): Line2
}

class HitscanRound(override val range: Float, override val damage: Float) : Round {
    override fun launch(
        sourceObject: GameObject,
        sourcePoint: Point2,
        direction: Point2,
        world: World,
        objects: Collection<GameObject>
    ): Line2 {
        val destination = sourcePoint + direction * range
        world.rayCast(sourcePoint, destination) { fixture, _, _, _ ->
            fixture.isNotSensor
        }?.let { result ->
                val obj = objects.find { result.fixture in it.body.fixtureList }
                if (obj is Character) {
                    obj.health -= damage
                    if (sourceObject is Player) sourceObject.score += if (obj.health > 0) 10 else 50
                }
                return sourcePoint to result.point.asPoint2
            } ?: return sourcePoint to destination
    }
}