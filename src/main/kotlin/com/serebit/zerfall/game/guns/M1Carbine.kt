package com.serebit.zerfall.game.guns

import com.serebit.zerfall.game.guns.components.*

class M1Carbine : Gun {
    override val name: String = "M1 Carbine"
    override val action: Action = SemiAutoAction(
        gunshotSoundPath = "sounds/m1carbine-gunshot.ogg",
        maxRoundsPerMinute = 375,
        round = HitscanRound(400f, 180f)
    )
    override val magazine: Magazine = BasicMagazine(
        magazineCapacity = 15,
        reserveCapacity = 90,
        reloadDuration = 2.7f,
        reloadSoundPath = "sounds/m1carbine-reload.ogg"
    )
}