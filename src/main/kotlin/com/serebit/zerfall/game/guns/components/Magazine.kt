package com.serebit.zerfall.game.guns.components

import com.badlogic.gdx.audio.Sound
import com.serebit.zerfall.utilities.Timer
import com.serebit.zerfall.utilities.assets.loadSound

interface Magazine {
    var magazineAmmo: Int
    var reserveAmmo: Int
    val magazineCapacity: Int
    val reserveCapacity: Int
    val reloadSound: Sound
    val reloadTimer: Timer

    fun refillReserves() {
        reserveAmmo = reserveCapacity
    }

    fun reload()

    fun update(delta: Float)
}

class BasicMagazine(
    override val magazineCapacity: Int,
    override val reserveCapacity: Int,
    reloadDuration: Float,
    reloadSoundPath: String
) : Magazine {
    override val reloadTimer = Timer(reloadDuration) {
        if (reserveAmmo > magazineCapacity) {
            reserveAmmo -= magazineCapacity
            magazineAmmo = magazineCapacity
        } else {
            magazineAmmo += reserveAmmo
            reserveAmmo = 0
        }
        reset()
    }
    override var magazineAmmo: Int = magazineCapacity
    override var reserveAmmo: Int = reserveCapacity
    override val reloadSound: Sound = loadSound(reloadSoundPath)

    override fun reload() {
        if (magazineAmmo < magazineCapacity && reserveAmmo > 0 && reloadTimer.isNotActive) {
            reserveAmmo += magazineAmmo
            magazineAmmo = 0
            reloadSound.play()
            reloadTimer.start()
        }
    }

    override fun update(delta: Float) {
        reloadTimer.update(delta)
    }
}