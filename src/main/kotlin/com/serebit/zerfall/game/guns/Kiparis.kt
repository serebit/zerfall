package com.serebit.zerfall.game.guns

import com.serebit.zerfall.game.guns.components.*

class Kiparis : Gun {
    override val name: String = "Kiparis"
    override val action: Action = FullAutoAction(
        "sounds/kiparis-gunshot.ogg",
        850,
        HitscanRound(200f, 80f)
    )
    override val magazine: Magazine = BasicMagazine(
        20,
        120,
        2.7f,
        "sounds/m1carbine-reload.ogg" // TODO: find kiparis reload sound
    )
}