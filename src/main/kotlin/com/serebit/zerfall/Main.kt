package com.serebit.zerfall

import com.badlogic.gdx.Files
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.serebit.loggerkt.LogLevel
import com.serebit.loggerkt.Logger

fun main(arg: Array<String>) {
    val config = LwjglApplicationConfiguration().apply {
        title = "Zerfall"
        width = 1280
        height = 720
        resizable = false
        addIcon("icons/zerfall.png", Files.FileType.Internal)
        useGL30 = true
        samples = 2
    }
    LwjglApplication(Application(), config)

    Logger.level = LogLevel.DEBUG
    // Prevents the application from crashing if unused variables are sent to the shader
    ShaderProgram.pedantic = false
}