package com.serebit.zerfall.utilities

class Timer(seconds: Float, private var callback: (Timer.() -> Unit)? = null) {
    var isActive: Boolean = false
        private set
    var isDone: Boolean = false
        private set
    val isNotActive get() = !isActive
    private var totalTimeInSeconds = seconds.coerceAtLeast(0f)
    private var remainingTimeInSeconds = totalTimeInSeconds

    fun start() {
        isActive = true
        isDone = false
    }

    fun update(delta: Float) {
        if (isActive) {
            remainingTimeInSeconds -= delta
            if (remainingTimeInSeconds <= 0) {
                callback?.invoke(this)
                isActive = false
                isDone = true
            }
        }
    }

    fun reset(seconds: Float = totalTimeInSeconds) {
        isActive = false
        isDone = false
        totalTimeInSeconds = seconds.coerceAtLeast(0f)
        remainingTimeInSeconds = totalTimeInSeconds
    }
}