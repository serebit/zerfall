package com.serebit.zerfall.utilities.assets

import com.badlogic.gdx.assets.AssetLoaderParameters
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.FileHandleResolver
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator

class AssetManager(
    resolver: FileHandleResolver = InternalFileHandleResolver()
) : AssetManager(resolver) {
    private val fontQueue = mutableListOf<BitmapFontLoadParameter>()
    private val fonts = mutableListOf<BitmapFontEntry>()

    fun getFont(
        name: String,
        size: Int
    ) = fonts.firstOrNull {
        it.name == name && it.size == size
    }?.font ?: BitmapFont()

    override fun finishLoading() {
        super.finishLoading()
        fontQueue.forEach {
            generateFont(it)
        }
    }

    private fun generateFont(
        identifier: BitmapFontLoadParameter
    ) {
        val generator = FreeTypeFontGenerator(fileHandleResolver.resolve(identifier.path))
        fonts += BitmapFontEntry(identifier.path, identifier.size, generator.generateFont(identifier.parameter))
    }

    fun loadFont(
        path: String,
        vararg sizes: Int,
        init: FreeTypeFontGenerator.FreeTypeFontParameter.() -> Unit = {}
    ) {
        sizes.forEach { size ->
            fontQueue += BitmapFontLoadParameter(
                path,
                size,
                FreeTypeFontGenerator.FreeTypeFontParameter().apply {
                    init()
                    this.size = size
                }
            )
        }
    }

    inline fun <reified T : Any> load(
        path: String,
        parameter: AssetLoaderParameters<T>? = null
    ) = load(path, T::class.java, parameter)

    private data class BitmapFontEntry(
        val path: String,
        val size: Int,
        val font: BitmapFont
    ) {
        val name: String = path.split("/").last().removeSuffix(".ttf")
    }

    private data class BitmapFontLoadParameter(
        val path: String,
        val size: Int,
        val parameter: FreeTypeFontGenerator.FreeTypeFontParameter = FreeTypeFontGenerator.FreeTypeFontParameter()
    )
}