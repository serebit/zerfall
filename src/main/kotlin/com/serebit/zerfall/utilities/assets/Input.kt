@file:JvmName("Input")

package com.serebit.zerfall.utilities.assets

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.loaders.resolvers.ExternalFileHandleResolver
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.serebit.loggerkt.Logger

val internalResolver = InternalFileHandleResolver()
val externalResolver = ExternalFileHandleResolver()

private val soundCache: MutableMap<String, Sound> = mutableMapOf()

fun readFile(path: String): String? = resolve(path, FileHandle::readString)

fun loadSound(path: String): Sound = soundCache.getOrPut(path) {
    resolve(path, Gdx.audio::newSound)!!
}

fun loadMusic(path: String): Music = resolve(path, Gdx.audio::newMusic)!!

fun loadShader(vertexPath: String, fragmentPath: String): ShaderProgram? {
    val vertexShader = readFile(vertexPath) ?: return null
    val fragmentShader = readFile(fragmentPath) ?: return null

    ShaderProgram(vertexShader, fragmentShader).let { shader ->
        if (shader.log.isNotEmpty()) Logger.debug(shader.log)
        return if (shader.isCompiled) shader else null
    }
}

private inline fun <T> resolve(path: String, loader: (FileHandle) -> T): T? {
    return if (path.startsWith("/")) loader(externalResolver.resolve(path))
    else loader(internalResolver.resolve(path))
}