package com.serebit.zerfall.utilities

enum class FilterGroup(val index: Short) {
    CHARACTER(-1)
}