package com.serebit.zerfall.utilities.graphics

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer
import com.badlogic.gdx.physics.box2d.World
import com.badlogic.gdx.utils.Align
import com.serebit.zerfall.utilities.assets.loadShader
import kotlin.math.max

class Renderer(private val camera: Camera) {
    private val defaultShader = loadShader(
        "shaders/default.vert",
        "shaders/default.frag"
    )!!
    private val shapeRenderer = ShapeRenderer(5000, defaultShader).apply {
        projectionMatrix = camera.combined
        setAutoShapeType(true)
    }
    private val spriteBatch = SpriteBatch(8191, defaultShader).apply {
        projectionMatrix = camera.combined
    }
    private val debugRenderer = Box2DDebugRenderer()
    private val defaultFont = BitmapFont()
    val position: Vector3 get() = camera.position
    val width: Int get() = Gdx.graphics.width
    val height: Int get() = Gdx.graphics.height

    fun update() {
        camera.update()
        shapeRenderer.projectionMatrix = camera.combined
        spriteBatch.projectionMatrix = camera.combined
    }

    private inline fun renderShape(color: Color, type: ShapeType, task: ShapeRenderer.() -> Unit) {
        shapeRenderer.color = color
        shapeRenderer.begin()
        shapeRenderer.set(type)
        shapeRenderer.task()
        shapeRenderer.end()
    }

    private inline fun renderSprite(color: Color = Color.WHITE, task: SpriteBatch.() -> Unit) {
        spriteBatch.color = color
        spriteBatch.begin()
        spriteBatch.task()
        spriteBatch.end()
    }

    fun gl(task: GL20.() -> Unit) {
        val batchIsDrawing = spriteBatch.isDrawing
        val shapeIsDrawing = shapeRenderer.isDrawing
        if (batchIsDrawing) spriteBatch.end()
        if (shapeIsDrawing) shapeRenderer.end()
        Gdx.gl.task()
        if (shapeIsDrawing) shapeRenderer.begin()
        if (batchIsDrawing) spriteBatch.begin()
    }

    fun drawDebug(world: World) {
        debugRenderer.render(world, camera.combined)
    }

    fun fillRect(
        x: Float, y: Float,
        width: Float, height: Float,
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        shapeRenderer.rect(x - width / 2, y - height / 2, width, height)
    }

    fun strokeRect(
        x: Float, y: Float,
        width: Float, height: Float,
        strokeWidth: Float = 0f,
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        val adjustedX = x - width / 2
        val adjustedY = y - height / 2
        shapeRenderer.rectLine(
            adjustedX - strokeWidth / 2, adjustedY,
            adjustedX + width + strokeWidth / 2, adjustedY,
            strokeWidth
        )
        shapeRenderer.rectLine(
            adjustedX + width, adjustedY,
            adjustedX + width, adjustedY + height,
            strokeWidth
        )
        shapeRenderer.rectLine(
            adjustedX + width + strokeWidth / 2, adjustedY + height,
            adjustedX - strokeWidth / 2, adjustedY + height,
            strokeWidth
        )
        shapeRenderer.rectLine(adjustedX, adjustedY + height, adjustedX, adjustedY, strokeWidth)
    }


    fun fillPolygon(
        vertices: FloatArray,
        offset: Int = 0,
        count: Int = vertices.size,
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        shapeRenderer.polygon(vertices, offset, count)
    }

    fun fillCircle(
        x: Float, y: Float,
        radius: Float,
        segments: Int = max(1, 6 * Math.cbrt(radius.toDouble()).toInt()),
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        shapeRenderer.circle(x, y, radius, segments)
    }

    fun fillEllipse(
        x: Float, y: Float,
        width: Float, height: Float,
        rotation: Float = 0f,
        segments: Int = (12 * Math.cbrt(max(width * 0.5, height * 0.5)).toFloat()).toInt(),
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        ellipse(x, y, width, height, rotation, segments)
    }

    fun drawArc(
        x: Float, y: Float,
        radius: Float,
        start: Float,
        degrees: Float,
        segments: Int = max(1, (6 * Math.cbrt(radius.toDouble()) * (degrees / 360)).toInt()),
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        arc(x, y, radius, start, degrees, segments)
    }

    fun drawLine(
        x0: Float, y0: Float,
        x1: Float, y1: Float,
        width: Float = 0f,
        color: Color = Color.WHITE
    ) = renderShape(color, ShapeType.Filled) {
        rectLine(x0, y0, x1, y1, width)
    }

    fun drawTexture(
        texture: Texture,
        x: Float, y: Float,
        width: Float, height: Float,
        originX: Float = 0f, originY: Float = 0f,
        scaleX: Float = 1f, scaleY: Float = 1f,
        rotation: Float = 0f,
        srcX: Int = 0, srcY: Int = 0,
        srcWidth: Int = texture.width, srcHeight: Int = texture.height,
        flipX: Boolean = false, flipY: Boolean = false,
        color: Color = Color.WHITE
    ) = renderSprite(color) {
        spriteBatch.draw(
            texture,
            x, y,
            originX, originY,
            width, height,
            scaleX, scaleY,
            rotation,
            srcX, srcY,
            srcWidth, srcHeight,
            flipX, flipY
        )
    }

    fun drawText(
        text: String,
        x: Float, y: Float,
        start: Int = 0, end: Int = text.length,
        targetWidth: Float = Gdx.graphics.width.toFloat(),
        horizontalAlignment: Int = Align.left,
        wrap: Boolean = false,
        truncate: String? = null,
        font: BitmapFont = defaultFont,
        color: Color = Color.WHITE
    ) = renderSprite(color) {
        val adjustedX = when (horizontalAlignment) {
            Align.center -> x - width / 2
            Align.right -> x - width
            else -> x
        }
        font.color = color
        font.draw(spriteBatch, text, adjustedX, y, start, end, targetWidth, horizontalAlignment, wrap, truncate)
    }
}