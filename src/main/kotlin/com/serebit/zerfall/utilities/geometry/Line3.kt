package com.serebit.zerfall.utilities.geometry

data class Line3(var start: Point3, var end: Point3)