package com.serebit.zerfall.utilities.geometry

data class Line2(var start: Point2, var end: Point2)