package com.serebit.zerfall.utilities.geometry

import com.badlogic.gdx.math.Vector2

data class Point2(var x: Float, var y: Float) {
    val asVector2 get() = Vector2(x, y)

    operator fun plus(other: Point2) = copy(
        x = x + other.x,
        y = y + other.y
    )

    operator fun plus(factor: Float) = copy(
        x = x + factor,
        y = y + factor
    )

    operator fun minus(other: Point2) = copy(
        x = x - other.x,
        y = y - other.y
    )

    operator fun times(other: Point2) = copy(
        x = x * other.x,
        y = y * other.y
    )

    operator fun times(factor: Float) = copy(
        x = x * factor,
        y = y * factor
    )

    infix fun to(other: Point2) = Line2(this, other)

    fun toPoint3(z: Float) = Point3(x, y, z)
}