package com.serebit.zerfall.utilities.geometry

import com.badlogic.gdx.math.Vector3

data class Point3(var x: Float, var y: Float, var z: Float) {
    val asVector3 get() = Vector3(x, y, z)

    infix fun to(other: Point3) =
        Line3(this, other)
}