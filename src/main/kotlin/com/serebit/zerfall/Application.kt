package com.serebit.zerfall

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.serebit.zerfall.game.Game
import com.serebit.zerfall.utilities.assets.AssetManager
import com.serebit.zerfall.game.maps.PrototypeMap
import com.serebit.zerfall.`interface`.PauseMenu

class Application : ApplicationAdapter() {
    private lateinit var game: Game
    private lateinit var pauseMenu: PauseMenu
    private val assetManager = AssetManager().apply {
        loadFont("fonts/FiraCode-Regular.ttf", 20, 24, 32, 48)
        loadFont("fonts/DKFaceYourFearsII.ttf", 72)
        load<Texture>("textures/test.png")
    }

    override fun create() {
        game = Game(PrototypeMap(), assetManager)
        pauseMenu = PauseMenu()
        assetManager.finishLoading()
    }

    override fun render() {
        if (pauseMenu.isNotVisible) {
            game.update(Gdx.graphics.deltaTime)
        }
        pauseMenu.update(Gdx.graphics.deltaTime)
        game.render()
        pauseMenu.render()
    }
}