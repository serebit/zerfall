package com.serebit.zerfall.`interface`

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin

val defaultSkin = Skin(Gdx.files.internal("skin/default.json"))
