package com.serebit.zerfall.`interface`

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.FitViewport
import com.serebit.extensions.gdx.isKeyJustPressed
import com.serebit.zerfall.utilities.graphics.Renderer
import com.serebit.zerfall.`interface`.dsl.*

class PauseMenu {
    private val camera = OrthographicCamera(
        Gdx.graphics.width.toFloat(),
        Gdx.graphics.height.toFloat()
    ).apply {
        update()
    }
    private val renderer = Renderer(camera)
    private val stage: Stage
    var isVisible = false
    val isNotVisible get() = !isVisible

    init {
        stage = stage(FitViewport(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat(), camera)) {
            verticalGroup {
                setFillParent(true)
                align(Align.center)
                space(8f)

                label("PAUSED", "title")
                textButton("RESUME") {
                    onChange {
                        this@PauseMenu.isVisible = false
                    }
                }
                textButton("EXIT") {
                    onChange {
                        Gdx.app.exit()
                    }
                }
            }
        }
        Gdx.input.inputProcessor = stage
    }

    fun update(delta: Float) {
        if (isKeyJustPressed(Input.Keys.ESCAPE)) isVisible = !isVisible
        if (isVisible) {
            stage.act(delta)
        }
    }

    fun render() {
        if (isVisible) {
            renderer.update()
            renderer.fillRect(
                0f,
                0f,
                Gdx.graphics.width * 2f,
                Gdx.graphics.height * 2f,
                color = Color(0x11111188)
            )
            stage.draw()
        }
    }
}