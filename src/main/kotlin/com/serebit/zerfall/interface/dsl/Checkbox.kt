package com.serebit.zerfall.`interface`.dsl

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.serebit.zerfall.`interface`.defaultSkin

private fun buildCheckBox(
    label: String = "",
    styleName: String = "default",
    init: CheckBox.() -> Unit
) = CheckBox(label, defaultSkin, styleName).apply(init)

fun CheckBox.onChange(listener: CheckBox.(ChangeListener.ChangeEvent) -> Unit) {
    addListener(object : ChangeListener() {
        override fun changed(event: ChangeEvent, actor: Actor) = listener(event)
    })
}

fun Table.checkBox(
    label: String = "",
    styleName: String = "default",
    init: CheckBox.() -> Unit = {}
) {
    add(buildCheckBox(label, styleName, init)).apply {
        maxWidth(this.prefWidth)
    }
}