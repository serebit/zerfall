package com.serebit.zerfall.`interface`.dsl

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.serebit.zerfall.`interface`.defaultSkin

fun buildLabel(
    text: String,
    styleName: String = "default",
    init: Label.() -> Unit
) = Label(text, defaultSkin, styleName).apply(init)

fun Group.label(
    text: String,
    styleName: String = "default",
    init: Label.() -> Unit = {}
) {
    addActor(buildLabel(text, styleName, init))
}

fun Table.label(
    text: String,
    styleName: String = "default",
    init: Label.() -> Unit = {}
) {
    add(buildLabel(text, styleName, init))
}