package com.serebit.zerfall.`interface`.dsl

import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Table

fun Group.table(init: Table.() -> Unit = {}) {
    addActor(Table().apply(init))
}

fun Table.row(init: Table.() -> Unit = {}) {
    init()
    row().pad(8f, 16f, 8f, 16f).fillX()
}