package com.serebit.zerfall.`interface`.dsl

import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup
import com.badlogic.gdx.utils.viewport.Viewport

fun stage(viewport: Viewport, init: Stage.() -> Unit) = Stage(viewport).apply(init)

inline fun Stage.action(crossinline action: (Float) -> Boolean) = addAction(object : Action() {
    override fun act(delta: Float) = action(delta)
})

inline fun Actor.action(crossinline action: (Float) -> Boolean) = addAction(object : Action() {
    override fun act(delta: Float) = action(delta)
})

fun Stage.verticalGroup(init: VerticalGroup.() -> Unit) = VerticalGroup().apply {
    init()
    this@verticalGroup.addActor(this)
}