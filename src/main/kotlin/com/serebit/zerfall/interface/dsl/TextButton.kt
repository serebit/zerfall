package com.serebit.zerfall.`interface`.dsl

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.serebit.zerfall.`interface`.defaultSkin

private fun buildTextButton(
    text: String,
    styleName: String = "default",
    init: TextButton.() -> Unit
) = TextButton(text, defaultSkin, styleName).apply(init)

fun TextButton.onChange(listener: TextButton.(ChangeListener.ChangeEvent) -> Unit) {
    addListener(object : ChangeListener() {
        override fun changed(event: ChangeEvent, actor: Actor) = listener(event)
    })
}

fun Group.textButton(
    text: String,
    styleName: String = "default",
    init: TextButton.() -> Unit = {}
) {
    addActor(buildTextButton(text, styleName, init))
}