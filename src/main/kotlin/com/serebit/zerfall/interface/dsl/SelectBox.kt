package com.serebit.zerfall.`interface`.dsl

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.serebit.zerfall.`interface`.defaultSkin

private fun buildSelectBox(
    items: List<String>,
    styleName: String = "default",
    init: SelectBox<String>.() -> Unit
) = SelectBox<String>(defaultSkin, styleName).apply {
    init()
    setItems(*items.toTypedArray())
}

fun SelectBox<String>.onChange(listener: SelectBox<String>.(ChangeListener.ChangeEvent) -> Unit) {
    addListener(object : ChangeListener() {
        override fun changed(event: ChangeEvent, actor: Actor) = listener(event)
    })
}

fun Table.selectBox(
    labels: List<String>,
    styleName: String = "default",
    init: SelectBox<String>.() -> Unit = {}
) {
    add(buildSelectBox(labels, styleName, init))
}