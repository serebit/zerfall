package com.serebit.extensions.box2d

import com.badlogic.gdx.physics.box2d.Filter
import com.serebit.zerfall.utilities.FilterGroup

var Filter.group: FilterGroup?
    get() = FilterGroup.values().find { it.index == groupIndex }
    set(value) {
        groupIndex = value?.index ?: 0
    }