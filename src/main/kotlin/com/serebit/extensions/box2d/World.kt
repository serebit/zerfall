package com.serebit.extensions.box2d

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.Fixture
import com.badlogic.gdx.physics.box2d.World
import com.serebit.zerfall.utilities.geometry.Point2
import ktx.box2d.rayCast

fun World.queryAABB(
    position: Point2,
    dimensions: Point2,
    callback: (Fixture) -> Boolean
) = QueryAABB(
    callback,
    position.x - dimensions.x / 2,
    position.y - dimensions.y / 2,
    position.x + dimensions.x / 2,
    position.y + dimensions.y / 2
)

val Fixture.isNotSensor get() = !isSensor

fun World.rayCast(
    start: Point2,
    end: Point2,
    predicate: (Fixture, Vector2, Vector2, Float) -> Boolean
): RaycastResult? {
    val results = mutableSetOf<RaycastResult>()
    rayCast(start.asVector2, end.asVector2) { fixture, point, normal, fraction ->
        if (predicate(fixture, point, normal, fraction)) {
            // create a deep copy of point to prevent modification by box2D. this is important, DO NOT DELETE
            val adjustedPoint = Vector2(point.x, point.y)
            results.add(RaycastResult(fixture, adjustedPoint, normal, fraction))
            1f
        } else -1f
    }
    return results.minBy { it.fraction }
}

data class RaycastResult(
    val fixture: Fixture,
    val point: Vector2,
    val normal: Vector2,
    val fraction: Float
)