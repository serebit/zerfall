package com.serebit.extensions.gdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

fun isKeyPressed(keyCode: Int = Input.Keys.ANY_KEY): Boolean = Gdx.input.isKeyPressed(keyCode)

fun isKeyJustPressed(keyCode: Int = Input.Keys.ANY_KEY): Boolean = Gdx.input.isKeyJustPressed(keyCode)

fun isKeyReleased(keyCode: Int = Input.Keys.ANY_KEY): Boolean = !Gdx.input.isKeyPressed(keyCode)