package com.serebit.extensions.gdx

import com.badlogic.gdx.graphics.GL20

fun GL20.glClearColor(hex: Int) = glClearColor(
    (hex and 0xFF000000.toInt() shr 24) / 255f,
    (hex and 0xFF0000 shr 16) / 255f,
    (hex and 0xFF00 shr 8) / 255f,
    (hex and 0xFF) / 255f
)