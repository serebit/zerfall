@file:JvmName("GeometryExtensions")

package com.serebit.extensions.gdx

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.serebit.zerfall.utilities.geometry.Point2
import com.serebit.zerfall.utilities.geometry.Point3

val Vector2.asPoint2 get() = Point2(x, y)

operator fun Vector2.plus(other: Point2) =
    Point2(x + other.x, y + other.y)

operator fun Vector2.minus(other: Point2) =
    Point2(x - other.x, y - other.y)

operator fun Vector2.times(other: Point2) =
    Point2(x * other.x, y * other.y)

val Vector3.asPoint3 get() = Point3(x, y, z)